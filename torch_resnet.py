import torch
from torchviz import make_dot, make_dot_from_trace

model = torch.hub.load('pytorch/vision:v0.5.0', 'resnet50', pretrained=True)

x = torch.zeros(1, 3, 224, 224, dtype=torch.float, requires_grad=False)
out = model(x)
dot = make_dot(out, params=dict(model.named_parameters()))
with open('torch_resnet_50.dot', 'w') as f:
    f.write(str(dot))

# cmd
# dot -Tsvg torch_resnet_50.dot > torch_resnet_50.svg
