from keras.applications.resnet50 import ResNet50
from keras.utils import plot_model

model = ResNet50()

plot_model(model, to_file='keras_resnet_50.png', show_shapes=True)
