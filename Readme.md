# Print and visualize pythorch vs keras
Studying how to printing and visualize resnet 50 into pythorch and keras

## Pythorch
```
python torch_resnet.py
dot -Tsvg torch_resnet_50.dot > torch_resnet_50.svg
pix torch_resnet_50.svg
```

## Keras
```
python keras_resnet.py
pix keras_resnet_50.png
```

